# Gitlab Project Configurator Test Example

[![pipeline status](https://gitlab.com/grouperenault/gpc_demo/gpc-demo-config/badges/master/pipeline.svg)](https://gitlab.com/grouperenault/gpc_demo/gpc-demo-config/commits/master)

- [GPC Source Code](https://gitlab.com/grouperenault/gitlab-project-configurator)
- [GPC Online Technical Documentation](https://grouperenault.gitlab.io/gitlab-project-configurator/docs/index.html)
- [GPC Example Configuration Project](https://gitlab.com/grouperenault/gpc_demo/gpc-demo-config/)

## Pipeline description

Pipeline is divided into 2 stages:

- Stage 1 only test the configuration, without applying on the Gitlab server.
  It is executed in merge requests's pipeline.
  It still requires a Gitlab Token.
- Stage 2 apply the change on the Gitlab server.
  It may perform destructive operations.
  It is only executed on the "master" and "tags" pipelines.

## Configuration file

See the policies defined in
[common_policies.yml](https://gitlab.com/grouperenault/gpc_demo/gpc-demo-config/blob/master/common_policies.yml).

See on while projects are applied which policy with the configuration file defined in
[my_config.yml](https://gitlab.com/grouperenault/gpc_demo/gpc-demo-config/blob/master/my_config.yml).

## Environment variables

You need to define at least one environment variable in your Config project:

- `GPC_GITLAB_TOKEN`: than holds the token of a user that is **maintainer**
  of the project your need to apply the configuration one.

  For this project, this token is linked to a dedicated user @grouperenault.gpcdemo that only has
  permissions on https://gitlab.com/groups/grouperenault/gpc_demo

**Note**: ensure the policy on your Config project is "private".
You may leak unwanted tokens (especially the Gitlab token) if you keep the
project "internal".
